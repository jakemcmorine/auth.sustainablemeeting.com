const userModel=require("../models").user
const settingModel=require("../models").setting
const Mail=require("../services/MailService")
const authService=require("../services/AuthService")
class UserRepository {
  //User Sign Up Module
  async createUser(data){
      try{
        let isUser =await userModel.findOne({where:{email:data.email}});
        if(isUser){
           throw "user email already exist";
        }else{
           let user =await userModel.create(data);
           console.log(user)
           let psdLink=await authService.createResetLink(user.dataValues)
           data['resetLink']=CONFIG.redirection+'/setNewpassword/'+psdLink
           let mail_send=await Mail.sendConfirmationEmail(data)
           console.log(mail_send)
           return user;
        }
        
      }catch(error){
        console.log(error)
          throw error;
      }
      
  }
  async searchUser(where){
    try{
      let users =await userModel.findAll(where);
         return users;
    }catch(error){
        throw error;
    }
 }
  async findOneUser(where){
    try{
      let user =await userModel.findOne(where);
      return user;
    }catch(error){
        throw error;
    }
  }
  async getUsers(data){
    try{
      let users =await userModel.findAll({where:{orginasation_id:data}});
         return users;
    }catch(error){
        throw error;
    }
 }
async findSetting(id){
  try{
    let userSetting =await settingModel.findOne({where:{user_id:id}});
    return userSetting;
  }catch(error){
    console.log(error)
      throw error;
  }
}
async updateUser(data,id){
  try{
    let userSetting =await settingModel.update(data,{where:{user_id:id}});
    return userSetting;
  }catch(error){
    console.log(error)
      throw error;
  }
}
async updateSetting(data,id){
  try{
    let userSetting =await settingModel.update(data,{where:{user_id:id}});
    return userSetting;
  }catch(error){
    console.log(error)
      throw error;
  }
}
async updateUser(data,where){
  try{
    let userSetting =await userModel.update(data,where);
    return userSetting;
  }catch(error){
    console.log(error)
      throw error;
  }
}
async createSetting(data){
  try{
    let userSetting =await settingModel.create(data);
    return userSetting;
  }catch(error){
    console.log(error)
      throw error;
  }
}
 async deleteUser(data){
  try{
    let users =await userModel.destroy(data);
    return users;
  }catch(error){
      throw error;
  }
  
}
  async bulkCreate(data){
    try{
      //console.log(data)
      let isUser =await userModel.bulkCreate(data,{ ignoreDuplicates: true });
      console.log(isUser)
      let duplicateRecords=[]
      for(var i=0;i<isUser.length;i++){
         let psdLink=await authService.createResetLink(isUser[i].dataValues)
         data[i]['resetLink']=CONFIG.redirection+'/setNewpassword/'+psdLink
         let mail_send= Mail.sendConfirmationEmail(data[i])
         if(isUser[i].id==null){
           duplicateRecords.push(isUser[i].email);
         }
      }
      return duplicateRecords;
    }catch(error){
      console.log(error)
      throw error

    }
    
  }

}

module.exports = new UserRepository();
const  bluebird    = require('bluebird');
const  crypto 	   = bluebird.promisifyAll(require('crypto'));
const  model       = require('../models/Model');
const  ejs         = require('ejs');
const  nodemailer  = require('nodemailer');
const  dotenv      = require('dotenv');

/**
 * Random Token Creation
 */

 async function  randomToken() {
    let bytes = await crypto.randomBytes(16);
    let token = await bytes.toString('hex');
    return token;
}

/**
 * Random Token Setting
 */

const setRandomToken = (token,email,collectionschema,field) => model[collectionschema].findOne({ email:email }).then((user) => {
    user.passwordResetToken = token;
    user.passwordResetExpires = Date.now() + 3600000; // 1 hour
    user = user.save();
    return user;
});

/**
 * Configuring SMTP Server
 */
const smtpTransport = nodemailer.createTransport({
    service: process.env.MAILER,
    auth: {
        user:  process.env.MAILUSER,
        pass: process.env.MAILPASSWORD
    }
});


/**
 * Regestration Email
 */


const sendRegistrationEmail = function(user,collectionschema,host) {
    var token = user.passwordResetToken;
    var templatePath = appRoot+'/views/account/registration.ejs';
    ejs.renderFile(templatePath, { link :'http://'+host+'/resetPassword/'+token+'/'+collectionschema,data:user.name}, function(err, templateData) {

      var mailOptions = {
            from: process.env.MAILDISPLAY,
            to: user.email,
            subject: "Welcome to PayGo!!",
            html: templateData
        };
        return new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, function (err, info) {
                if (err) {
                    reject(new Error('Error Message'))
                }

            });
        });
    });
};



/**
 * Regestration and password Reset
 */

exports.mailer = async(email,collectionschema,field,host,callback) => {
    try {
        var token  =  await randomToken();
        var user   =  await setRandomToken(token,email,collectionschema,field);
        await sendRegistrationEmail(user,collectionschema,host);
        await callback({"success":1})
    }catch (err){
        console.log(err);
        callback({"success":0});
    }
}

/**
 * Send Mail
 */
exports.sendMail = function(data,callback){
    var emailSub    = '';
    var emailBody   = '';
    var stockType   = data.stockType;

    if(data.stockAction==='StockTransfer') {
        emailSub    = `PayGo: Stock Transfer - ${stockType}`;
        emailBody   = ` <html>
                            <br>Hello Admin,
                            <br><br>A stock transfer - ${data.stockType} amount of ${data.stockAmount} UGX has been initiated via PayGo.
                            <br>Please review this request as soon as possible.
                            <br><br><br>Regards,
                            <br>PayGo Support Team
                            <br>Plot 46,Mirembe Business Centre
                            <br>Lugogo Bypass<br>Kampala
                            <br><a href="www.paygo.mobi">www.paygo.mobi</a>
                            <br>
                        </html>`;
    } else if(data.stockAction==='StockApproval') {
        emailSub    = `PayGo: Stock Approval - ${stockType}`;
        emailBody   = ` <html>
                            <br>Hello ${data.email[0]},
                            <br><br>A ${data.stockType} amount of ${data.stockAmount} UGX  has been approved for you.
                            <br><br>If you need any further help please reach out to us on 0700798100 /  0200907000 or email us at support@paygo.mobi
                            <br><br><br>Regards,
                            <br>PayGo Support Team
                            <br>Plot 46,Mirembe Business Centre
                            <br>Lugogo Bypass<br>Kampala
                            <br><a href="www.paygo.mobi">www.paygo.mobi</a>
                            <br>
                        </html>`;
    }

    var mailOptions={
        from : process.env.MAILDISPLAY,
        to : data.email,
        subject : emailSub,
        html : emailBody,
    }
    smtpTransport.sendMail(mailOptions, function(error, res){
    	if(error){
            callback({"success":"0"})
        }
        else{
            callback({"success":"1"})
        }
    });
}

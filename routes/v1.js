const express 			= require('express');
const router 			= express.Router();
const uploadData         = require("../middleware/multer");
const UserController 	= require('../controllers/userController');
const HomeController 	= require('./../controllers/HomeController');
const passport      	= require('./../middleware/passport');
const path              = require('path');
var multer = require('multer');
const upload = multer({storage:uploadData.storage});
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"Parcel Pending API", data:{"version_number":"v1.0.0"}})
});
router.post('/regenerateTocken',UserController.regenerateTocken);   
router.post('/updatePassword', UserController.updatePassword);    
router.post('/logout',passport.checkjwtToken,UserController.logout);   
router.post('/getUsers',UserController.getUsers);  
router.post('/changepassword', passport.checkjwtToken,UserController.changePassword);    
router.post('/emailChange', passport.checkjwtToken,UserController.emailChange);
//router.post('/deactivate', passport.authenticate('jwt', {session:false}), UserController.deactivateUser);  
//router.post('/socialLoginFb',        UserController.socialLoginFb);        
//router.post('/deactivateUser', UserController.deactivate);        
//router.post('/updateFbEmailLogin',UserController.socialLoginFbUpdate);        
//router.post('/socialLogin',UserController.socialLogin);        
router.post('/sendPasswordResetLink',UserController.sendPasswordResetLink);          
//router.post('/users',           UserController.create);   
router.get('/email/verify/:tocken',UserController.emailVarify);         
router.post('/email/verify',    UserController.emailVarify);                                              // C
//passport.authenticate('jwt', {session:false}), UserController.myprofile);        // R
//outer.delete('/users',passport.authenticate('jwt', {session:false}), UserController.remove);     // D
router.post('/login',     UserController.login);
//router.get('/dash', passport.authenticate('jwt', {session:false}),HomeController.Dashboard) 
router.post('/getAccountDet', passport.checkjwtToken,UserController.getAccountDet);
router.post('/inviteUsers', passport.checkjwtToken,UserController.inviteuser);
router.post('/saveDetail',passport.checkjwtToken,UserController.saveDetail);
router.post('/bulkuUserinvite',passport.checkjwtToken,upload.single("file"),UserController.bulkInvite);
router.get('/getAllUser',passport.checkjwtToken,UserController.getAllUser);
router.post('/deleteUser',passport.checkjwtToken,UserController.deleteUser); 
router.post('/updateUserSetting',passport.checkjwtToken,UserController.updateUserSetting);
router.post('/getUserSetting',passport.checkjwtToken,UserController.getUserSetting);
// router.post('/bulkUploadQuestionsToModule', function (req, res, next) {   
//   uploadData.upload(req, res, function (err) {
//       if (err) { getAllUser
//           console.log(err)
//           if (err.type == "file_type_error") {
//               return res.status(422).json({ "result": "error", "msg": "File Format Not Supported"})
//           }
//           else if (err.code == "LIMIT_FILE_SIZE") {
//               return res.status(413).json({"result": "error", "msg": "File Size Exceeded.The Maximum Uploadable File Size is 1MB"})
//           }
//           else{
//               return res.status(400).json({"result": "error", "msg": "UnExpected File Upload Error"})
//           }
//       }
//       else{
//           next();
//       }
      
//   });
// }, excelValidations.excelValidation, questionController.bulkUploadQuestions);
//********* API DOCUMENTATION **********
router.use('/docs/api.json',            express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
router.use('/docs',                     express.static(path.join(__dirname, '/../public/v1/documentation/dist')));
module.exports = router;

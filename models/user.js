'use strict';
let bcrypt=require('bcrypt')
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    webUrl: DataTypes.STRING,
    phone: DataTypes.STRING,
    orginasation_id:DataTypes.INTEGER,
    email:{
      unique: true,
      type:DataTypes.STRING,
    }, 
    password: DataTypes.STRING,
    status: DataTypes.STRING,
    type: DataTypes.STRING
  }, {});
  user.associate = function(models) {
    // associations can be defined here
  };
  user.prototype.compare = async function(password){
    
    let psd_check= bcrypt.compare(password, this.password);
    return psd_check;
  }

  return user;
};
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      emailSend: {
        type: Sequelize.STRING
      },
      hours: {
        type: Sequelize.INTEGER
      },
      minute: {
        type: Sequelize.INTEGER
      },
      exclude_meeting: {
        type: Sequelize.STRING
      },
      info_url: {
        type: Sequelize.STRING
      },
      footer: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Settings');
  }
};
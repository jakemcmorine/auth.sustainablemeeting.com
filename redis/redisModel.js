"use strict";
const {promisify} = require('util');

class redisModel {
	constructor(client){
		this.getAsync = promisify(client.get).bind(client);
		this.setAsync = promisify(client.set).bind(client);
		this.hmset = promisify(client.hmset).bind(client);
		this.hgetall = promisify(client.hgetall).bind(client);
		this.exists = promisify(client.exists).bind(client);
		this.del = promisify(client.del).bind(client);
		this.expire=promisify(client.expire).bind(client);
		this.lpush=promisify(client.lpush).bind(client);
		this.lrange=promisify(client.lrange).bind(client);
		this.lrem=promisify(client.lrem).bind(client);
	}
}
module.exports =  redisModel;
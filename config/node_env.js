 if(process.env.NODE_ENV =="dev"){
    var dotenv = require('dotenv').config({ path: '.env.dev' });

    // dotenv.load();
   }else if(process.env.NODE_ENV =="local"){
   // dotenv.load({ path: '.env.production' });
     console.log("development");
     var dotenv = require('dotenv').config({ path: '.env.local' });
   }else if(process.env.NODE_ENV =="test"){
    // dotenv.load({ path: '.env.production' });
    // console.log("development");
      var dotenv = require('dotenv').config({ path: '.env.test' });
    }else if(process.env.NODE_ENV =="prod"){
      var dotenv = require('dotenv').config({ path: '.env.production' });
    }
//instatiate environment variables

CONFIG = {} //Make this global to use all over the application

CONFIG.app          = process.env.APP   ;
CONFIG.port         = process.env.PORT  ;
CONFIG.newrelic         = process.env.NEW_RELIC  ;
CONFIG.newrelic_name         = process.env.NEW_RELIC_NAME  ;

CONFIG.mongourl     =  process.env.MONGO_URL

CONFIG.redis_host      = process.env.REDIS_HOST       ;
CONFIG.redis_port      = process.env.REDIS_PORT      ;

CONFIG.mail_username   = process.env.MAIL_USERNAME    ;
CONFIG.mail_password      = process.env.MAIL_PASSWORD      ;
CONFIG.mail_from       = process.env.MAIL_FROM       ;
CONFIG.mail_host            =  process.env.MAIL_HOST;

CONFIG.aws_secret       =process.env.AWS_SECRET;
CONFIG.aws_accessKeyId  = process.env.aws_accessKeyId ;

CONFIG.redirection     =process.env.REDIRECTION_URL;
CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION || 'mJV6BZCqU3FAkkxX2ABsmnW5hMS43pVN';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '100000000000';
CONFIG.AUTH_URL       = process.env.AUTH_URL;
CONFIG.username     =process.env.username;
CONFIG.password  = process.env.password || 'mJV6BZCqU3FAkkxX2ABsmnW5hMS43pVN';
CONFIG.database  = process.env.database || '100000000000';
CONFIG.host     =process.env.host;
CONFIG.SUPERADMIN_URL     =process.env.SUPERADMIN_URL;
CONFIG.ADMIN_URL     =process.env.ADMIN_URL;
CONFIG.dialect  = process.env.dialect || 'mJV6BZCqU3FAkkxX2ABsmnW5hMS43pVN';
CONFIG.operatorsAliases  = process.env.operatorsAliases || '100000000000';

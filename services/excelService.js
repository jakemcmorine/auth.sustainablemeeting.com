var node_xj = require("xls-to-json");
const path = require('path');
var util = require('util');
var xj = util.promisify(node_xj)
var Excel = require('exceljs');
var XLSX = require('xlsx');
var fs = require('fs');
const { promisify } = require('util');
const deleteFileAsync = promisify(fs.unlinkSync)

module.exports.readExcel = async (filePath) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filePath);
        let excelReadedData = await xj({
            input: paths,  // input xls
            output: null, // output json
            rowsToSkip: 0 // number of rows to skip at the top of the sheet; defaults to 0 
        });
        return ({ "result": "success", "data": excelReadedData })
    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": "" })
    }


}

module.exports.checkExcelHeaders = async (filePath) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filePath);
        var workbook = XLSX.readFile(paths);
        var sheet_name_list = workbook.SheetNames;
        let outPutJSON = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
        return ({ "result": "success", "data": outPutJSON })

    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": "" })
    }
}

module.exports.deleteExcelFile = async (filename) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filename);
        // delete file named 'sample.txt' Synchronously
        let deletedStatus = await fs.unlinkSync(paths);
        return ({ "result": "success", "data": "deletedStatus" })
    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": err })

    }
}
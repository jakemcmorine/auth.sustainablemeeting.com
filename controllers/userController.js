const authService   = require('../services/AuthService');
const redisSession   = require('../services/RedisSessionService');
const jsonconvertor   = require('../services/excel-to-json');
const uuid   = require('uniqid');
const bcrypt 			= require('bcrypt');
const bcrypt_p 			= require('bcrypt-promise');
const jwt           	= require('jsonwebtoken');
const userRepository= require("../repositories/userRepository")
var Mail 	        = require('../services/MailService');

const create = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    body.status="firstStep";
    if( !body.email && !body.phone){
        CRM.saveItem(req,"registration","fail","wrong email");
        return ReE(res, 'Please enter an email or phone number to register.');
    } else if(!body.password){
        CRM.saveItem(req,"registration","fail","wrong password")
        return ReE(res, 'Please enter a password to register.');
    }else{
        let err, user;
        [err, user] = await to(authService.createUser(body,req));
        if(err) return ReE(res, err, 422);
        [err, key] = await to(redisSession.createSessionForRegister(user.toJSON()));
        if(key){
            [err, suc] = await to(redisSession.tokenExpireTime(key,301));
            if(suc)
                [err, sccs] = await to(Mail.sendConfirmationEmail(key,user.toJSON()));
            if(sccs){
                CRM.saveItem(req,"registration","success","success")
                return ReS(res, {message:'We have sent an email. Please verify your email for completing registration process.',success:true}, 201);
            }
                
            if(err){
                CRM.saveItem(req,"registration","fail",err)
                console.log(err);
            }
        }
         if(err){
            console.log(err);
         }
    }
}
module.exports.create = create;
const sendPasswordResetLink = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    if( !body.email){
        return ReE(res, 'Please enter an email or phone number to register.');
    } else{
        try{
            let err, user;
            user = await userRepository.searchUser({where:{email:body.email}});
            if(user.length>0) {
                key  = await redisSession.createSessionForRegister(user[0].dataValues);
                suc =  await redisSession.tokenExpireTime(key,2000000);
                sccs = await  Mail.sendForgotPasswordLink(key,user[0].dataValues);
                return ReS(res, {message:'We have sent Password reset link to your Email Id.'}, 201);
            }
            else{
                return ReE(res, {"message":"The email address that you've entered is  not registered"}, 422);
            }
        }catch(error){
            res.statusCode = 422;
            console.log(error)
           return res.json({message:error})
        }
    }
}
module.exports.sendPasswordResetLink = sendPasswordResetLink;
const emailChange = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    if( !body.email){
        return ReE(res, 'Please enter an email');
    } else{
        try{
            if(req.email!=body.email){
                let err, user;
                let check = await userRepository.searchUser({where:{email:body.email}});
                if(check.length==0) {
                    user = await userRepository.searchUser({where:{email:req.email}});
                    user[0].dataValues['email']=body.email;
                    console.log('emailllll',user[0].dataValues['email'])
                    key  = await redisSession.createSessionForRegister(user[0].dataValues);
                    suc =  await redisSession.tokenExpireTime(key,2000000);
                    sccs = await  Mail.sendEmailVarifyLink(key,user[0].dataValues);
                    return ReS(res, {message:'We have sent Email confirmation link to your Email Id.'}, 201);
                }
                else{
                    return ReE(res, {"message":"The email address that you've entered is registered"}, 422);
                }

            }else{
                return ReE(res, {"message":"You have not changed email"}, 422);
            }
            
        }catch(error){
            res.statusCode = 422;
            console.log(error)
           return res.json({message:error})
        }
    }
}
module.exports.emailChange = emailChange;
const emailVarify = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    if(!req.params.tocken){
        return ReE(res,{"message":"Invalid Link"},422) 
    } else{
        let err, user,data;
        [jwtError,key]=await to(authService.extractJWT(req.params.tocken));
        if(key){
            [err, userEmailSession] = await to(redisSession.findSession(key.user_id));
             console.log(userEmailSession)
             if(userEmailSession){
               let user = await userRepository.updateUser({email:userEmailSession.email},{where:{id:userEmailSession.id}});
                [err, userEmailSession] = await to(redisSession.deleteSession(key.user_id));
                return ReS(res, {message:'Your email changed successfully.Please login with new credentials',success:true}, 201);
            }else{
               // return ReE(res, 'Invalid Link');
                return ReE(res,{"message":"Invalid Link"},422) 

            }
        }else{
            return ReE(res,{"message":"Invalid Link"},422) 
        }
    }
}
module.exports.emailVarify = emailVarify;


const logout = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    let user = req.user;
    try{
        user = await authService.logoutUser(req.userId);
        return ReS(res, {message:'You have been logged out',success:true}, 201);
    }catch(error){
        return ReE(res,{"message":"Invalid"},422)
    }
     
    
}
module.exports.logout = logout;
const updatePassword = async function(req, res){
    res.setHeader('Content-Type', 'application/json');
    const body = req.body;
    if(!body.emailTocken){
        return ReE(res, 'Invalid request..!');
    } else{
        try{
            let err, user,data;
            let key= await authService.extractJWT(body.emailTocken);
            let userEmailSession = await redisSession.findSession(key.user_id);
            console.log(userEmailSession)
            if(userEmailSession!=null){
                let salt = await bcrypt.genSalt(10);
                let hash = await bcrypt.hash(body.password, salt);
                console.log(hash)
                let user = await userRepository.updateUser({password:hash},{where:{id:userEmailSession.id}});
                let deleteSession = await redisSession.deleteSession(key.user_id);
                return ReS(res, {message :'Your password has been changed successfully. Please login with your new credentials'},201);
            }
            else return ReE(res,{"message":"Reset Link has expired"},400) 

        }catch(error){
            console.log(error)
            return ReE(res,{"message":"Reset Link has expired"},400) 
        }
      
    }
}
module.exports.updatePassword = updatePassword;
const myprofile = async function(req, res){
    let myProfile,error;
    res.setHeader('Content-Type', 'application/json');
   // let user = req.user;
     myProfile  = await User.find({"id":req.userId});
    if(myProfile)
       return ReS(res, {user:myProfile,success:true});
    if(err)
        return ReE(res, err);
}
module.exports.myprofile = myprofile;  
const getUsers = async function(req, res){
    try{
        let err, users, data
        data=req.body;
        user = req.user;
        data.status="completed";
        users = await userRepository.searchUser({where:{type:'admin'}});
        return ReS(res, {data:users});
    }catch(error){
         return ReE(res, error);
    }
}
module.exports.getUsers=getUsers;
const getAccountDet = async function(req, res){
    try{
        let err, user, data
        data=req.body;
        user = req.user;
        data.status="completed";
        user = await userRepository.findOneUser({where:{id:req.userId}});
        let userDet={};
        userDet.firstName=user.dataValues.firstName;
        userDet.lastName=user.dataValues.lastName;
        userDet.webUrl=user.dataValues.webUrl;
        userDet.email=user.dataValues.email;
        userDet.phone=user.dataValues.phone;
        return ReS(res, {message:userDet});

    }catch(error){
         return ReE(res, error);
    }
    
}
module.exports.getAccountDet=getAccountDet;
const saveDetail = async function(req, res){
    try{
        let err, user, data
        data=req.body;
        user = req.user;
        data.status="completed";
        user = await userRepository.updateUser(data,{where:{id:req.userId}});
        return ReS(res, {message :'Data updated successfully'});

    }catch(error){
         return ReE(res, error);
    }
    
}
module.exports.saveDetail = saveDetail;

const deactivateUser = async function(req, res){
    let err, user, data
    data=req.body;
    user = req.user;
    [err, user] = await to(User.update({"_id":user.id},{"deleted_at":1}));
    if(err){
       //  err = 'Db error';
        return ReE(res, err);
    }else{
        [err, user] = await to(authService.logoutUser(user.user_id));
        return ReS(res, {message:'You have been deactivated',success:true}, 201);
    }
}
module.exports.deactivateUser = deactivateUser;
// const deactivate = async function(req, res){
//     let err, user, data
//     data=req.body;
    
//     data.status="completed";
//     [err, user] = await to(redisSession.deletesessions(data.id));
//     if(err){
//        //  err = 'Db error';
//         return ReE(res, err);
//     }
//   //  return ReS(res, {message :'Updated User: '+user.email});
// }
// module.exports.deactivate = deactivate;
const changePassword= async function(req,res){
    res.setHeader('Content-Type', 'application/json');
    let userId = req.userId;
    let myData;
    let updatePsd
    let data=req.body;
    try{
        myData = await userRepository.findOneUser({where:{"id":userId}});
        if(myData){
            updatePsd = await bcrypt_p.compare(data.oldPsd,myData.dataValues.password);
            if(!updatePsd){
                return ReE(res, {message:"Incorrect Current Password"}, 422);
            }
            if(updatePsd){
                let salt = await bcrypt.genSalt(10);
                let hash = await bcrypt.hash(data.newPsd, salt);
                myData   = await userRepository.updateUser({password:hash},{where:{"id":userId}});
                return ReS(res, {  message:"You have successfully changed password",success:true}, 201);
            }
        }
   }catch(error){
       console.log(error)
       return ReE(res,{"message":"something went wrong try again"},400) 
   }
    
   
    
    
}
module.exports.changePassword = changePassword;

const remove = async function(req, res){
    let user, err;
    user = req.user;

    [err, user] = await to(user.destroy());
    if(err) return ReE(res, 'error occured trying to delete user');

    return ReS(res, {message:'Deleted User'}, 204);
}
module.exports.remove = remove;
const regenerateTocken = async function(req, res){
    let session, jwtError,key,err;
    let tocken = req.body.tocken;

    if(tocken==""){
        [err,session] = await to(authService.createAnonymusUser());
        return ReS(res, {tocken :session},201);
    }else{
        [jwtError,key]=await to(authService.extractJWT(tocken));
        if(jwtError){
            [err,session] = await to(authService.createAnonymusUser());
            return ReS(res, {tocken :session},201);

        }else{
            [err,session] = await to(redisSession.findSession(key));
            if(err){
                [err, session] = await to(authService.createAnonymusUser());
                return ReS(res, {tocken :session},201);
            }else{
                return ReS(res, {tocken :tocken},201);
            }
        }
        
    }
}
module.exports.regenerateTocken = regenerateTocken;
const login = async function(req, res){
    const body = req.body;
    let err, user;
  // console.log("dsfkjdsahflkjhzslkjdfhadskjfhlakdsjfhldsakjhf")
    try{
          user = await authService.authUser(body);
          console.log(user)
          let key=await redisSession.createSessionForLogin(user)
          res.statusCode = 200;
          res.json({  message:"You have successfully logged in",success:true,provider:user.provider,fname:user.fname,type:user.type,token:jwt.sign({user_id:key}, CONFIG.jwt_encryption, {expiresIn:20000000}),email:user.email})
    }catch(error){
        res.statusCode = 422;
        return res.json({message:error})
    }
}
module.exports.login = login;
const updateUserSetting = async function(req, res){
    const body = req.body;
    let err, user;
    try{
          user = await userRepository.findSetting(req.userId);
          if(user){
            await userRepository.updateSetting(body,req.userId);
          }else{
            body['user_id']=req.userId;
            await userRepository.createSetting(body);
          }
          res.json({  message:"successfully updated settings",success:true})
    }catch(error){
        res.statusCode = 422;
        return res.json({message:error})
    }
} 
module.exports.updateUserSetting = updateUserSetting;
const getUserSetting = async function(req, res){
    const body = req.body;
    let err, user;
    try{
          user = await userRepository.findSetting(req.userId);
          res.json({ message:user,success:true})
    }catch(error){
        res.statusCode = 422;
        return res.json({message:error})
    }
} 
module.exports.getUserSetting = getUserSetting;
const bulkInvite=async function(req,res){
    try{
        console.log(req.userId)
        let filename=req.file.filename;
        let json= await jsonconvertor.convertToJson(filename)
        let psd,salt,has_psd;
        for(let i=0;i<json.length;i++){
            psd=uuid();
            salt = await bcrypt.genSalt(10);
            has_psd = await bcrypt.hash(psd,salt);
            json[i]['password']=has_psd;
            json[i]['type']="employee";
            json[i]['orginasation_id']=req.userId; 
        }
        let create=await userRepository.bulkCreate(json);
        let data_uploaded=json.length-create.length;
        let duplicate_data=create.length;
        return res.json({success:"Data uploaded "+data_uploaded+" . duplicated data "+duplicate_data })
        // console.log(json)
            //return json

     }catch(error){
         console.log(error)
        res.statusCode = 422;
        return res.json({message:error})
     }
   
}
module.exports.bulkInvite=bulkInvite;
const inviteuser = async function(req,res){
    const body = req.body;
    try{
        let psd=uuid();
        let salt = await bcrypt.genSalt(10);
        let has_psd = await bcrypt.hash(psd,salt);
        let data={firstName:body.first_name,orginasation_id:req.userId,lastName:body.last_name,type:"employee",email:body.email,password:has_psd,"original_password":psd};
        let create=await  userRepository.createUser(data)
       res.json({success:true})
    }catch(error){
       console.log(error)
      res.statusCode = 422;
       res.json({error:error})
    }
    
}
module.exports.inviteuser = inviteuser;


const getAllUser = async function(req,res){
    const body = req.body;
    try{
         let datas=await  userRepository.getUsers(req.userId);
         res.json({data:datas})
    }catch(error){
       console.log(error)
      res.statusCode = 422;
       res.json({error:error})
    }
    
}
module.exports.getAllUser = getAllUser;
const deleteUser = async function(req,res){
    const body = req.body.id;
    try{
         let datas=await  userRepository.deleteUser({where:{id:body}});
         res.json({data:datas})
    }catch(error){
       res.statusCode = 422;
       res.json({error:error})
    }
    
}
module.exports.deleteUser = deleteUser;





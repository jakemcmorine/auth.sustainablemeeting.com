var node_xj = require("xls-to-json");
const path = require('path');
var util = require('util');

var XLSX = require('xlsx');
var fs = require('fs');
let csvToJson = require('convert-csv-to-json');

const { promisify } = require('util');
const deleteFileAsync = promisify(fs.unlinkSync)

module.exports.readExcel = async (filePath) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filePath);
        let json = await csvToJson.getJsonFromCsv(paths);
        return json
    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": "" })
    }


}

module.exports.convertToJson = async (filePath) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filePath);
        var workbook = XLSX.readFile(paths);
        var sheet_name_list = workbook.SheetNames;
        let outPutJSON = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
        console.log(outPutJSON)
        return outPutJSON 

    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": "" })
    }
}

module.exports.deleteExcelFile = async (filename) => {
    try {
        let paths = path.join(__dirname, '..', 'uploads', filename);
        // delete file named 'sample.txt' Synchronously
        let deletedStatus = await fs.unlinkSync(paths);
        return ({ "result": "success", "data": "deletedStatus" })
    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": err })

    }
}
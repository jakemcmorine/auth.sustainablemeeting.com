var fs        = require('fs');
var path      = require('path');
var basename  = path.basename(__filename);
var redis_models    = {};
var redis = require("redis");
    //client = redis.createClient();
var client;
if(CONFIG.redis_host != ''){
    console.log(CONFIG.redis_host);
    client = redis.createClient({host:CONFIG.redis_host, port:CONFIG.redis_host });
   
    const {promisify} = require('util');
    
    client.on("error", function (err) {
        console.log("Error conecting to redis server");
    });
    client.on("connect", function (success) {
        console.log("connected to redis server");
    });
    
    // End of Mongoose Setup
}else{
    console.log("No redis Credentials Given");
}

module.exports = client;

const User 			= require('./../models').user;
const validator     = require('validator');
const jwt           	= require('jsonwebtoken');
const redisSession   = require('./../services/RedisSessionService');

const getUniqueKeyFromBody = function(body){// this is so they can send in 3 options unique_key, email, or phone and it will work
   console.log(body)
    let unique_key = body.unique_key;
    if(typeof unique_key==='undefined'){
        if(typeof body.email != 'undefined'){
            unique_key = body.email
        }else if(typeof body.phone != 'undefined'){
            unique_key = body.phone
        }else{
            unique_key = null;
        }
    }
    return unique_key;
}
module.exports.getUniqueKeyFromBody = getUniqueKeyFromBody;
const createResetLink=async function(data){
    let key  = await redisSession.createSessionForRegister(data);
    let suc =  await redisSession.tokenExpireTime(key,2000000);
    key=jwt.sign({user_id:key}, CONFIG.jwt_encryption,{expiresIn:30001})
    return key;
}
module.exports.createResetLink = createResetLink;

const createUser = async function(userInfo,req){
    let unique_key, auth_info, err;

    auth_info={}
    auth_info.status='create';

    unique_key = getUniqueKeyFromBody(userInfo);
    if(!unique_key){
        TE('An email or phone number was not entered.');
    }
    if(validator.isEmail(unique_key)){
        auth_info.method = 'email';
        userInfo.email = unique_key;
        userInfo.isEmailVarified=false;
        [err, user] = await to(User.create(userInfo));
        if(err){
            TE('User already exists with the same Email Id');
        } 
        return user;
    }else if(validator.isMobilePhone(unique_key, 'any')){//checks if only phone number was sent
        auth_info.method = 'phone';
        userInfo.phone = unique_key;

        [err, user] = await to(User.create(userInfo));
        if(err) TE('User already exists with that phone number');

        return user;
    }else{
        TE('A valid email or phone number was not entered.');
    }
}
module.exports.createUser = createUser;
const extractJWT = async function(key){
  //  let expiration_time = parseInt(CONFIG.jwt_expiration);
    decoded = jwt.verify(key, CONFIG.jwt_encryption);
    return decoded;
  //  return "Bearer "+jwt.sign({user_id:key}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
};
module.exports.extractJWT = extractJWT;
createAnonymusUser=async function(){
    [sessionError,key]=await to(redisSession.createSessionForRegister({email:"anonymus",_id:"anonymus"}));
    var encryptKey= jwt.sign({user_id:key}, CONFIG.jwt_encryption,{expiresIn:30000})
    return encryptKey;
}
module.exports.createAnonymusUser = createAnonymusUser;
const logoutUser =async function(key){
    var opts = {};
    [err, userDelete] = await to(redisSession.deleteSession(key));
};
module.exports.logoutUser = logoutUser;
const authUser = async function(userInfo){//returns token
    try{
    let user= await User.findOne({where:{email:userInfo.email }});
    let unique_key;
    let auth_info = {};
    auth_info.status = 'login';
    unique_key = getUniqueKeyFromBody(userInfo);
 
    if(!unique_key) throw 'Please enter an email or phone number to login';
 
 
    if(!userInfo.password) throw 'Please enter a password to login';
 
    // if(validator.isEmail(unique_key)){
    //     auth_info.method='email';
    //      user = await User.findOne({where:{email:unique_key }});
    //     //if(err) TE(err.message);
 
    // }else{
    //     throw 'A valid email or phone number was not entered';
    // }
 
    if(!user) throw 'Unregistered Email Id';
    
    let psdcheck =  await user.compare(userInfo.password);
    if(psdcheck){
        return user.dataValues;
    }else{
        throw 'Invalid password'
    }
    //console.log(psdcheck);
   // if(err) TE(err.message);
    
   // return user;
 
   }catch(error){
       console.log(error)
      throw error
   }
    
  

}
module.exports.authUser = authUser;
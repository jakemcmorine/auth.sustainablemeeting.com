const validator     = require('validator');
var randomstring    = require("randomstring");
var Redis 			=  require('./../redis/');
var RedisClient 	 = require('./../redis/redisModel');

RedisClient= new RedisClient(Redis);
const createSessionForRegister = async function(userInfo){
    try{
        var key = randomstring.generate(4);
        var email =userInfo.email;
        let success = await RedisClient.hmset(key,"email",email,"id",userInfo.id);
         success    = await RedisClient.lpush(userInfo.id,key);
        return key;
    }catch(error){
        throw error;
    }
} 
module.exports.createSessionForRegister = createSessionForRegister;
const createFlashMessage = async function(id,message,status){
    try{
        var key = randomstring.generate(4);
        let success = await RedisClient.hmset(id,"message",message,"status",status);
        return key;
    }catch(error){
        throw error;
    }
} 
module.exports.createFlashMessage = createFlashMessage;
 const tokenExpireTime = async function(key,expireInseconds){//returns token
    try{
        let success = await RedisClient.expire(key,expireInseconds);
        return success;
    }
    catch(error){
        throw error;
    }
  
    
}

module.exports.tokenExpireTime = tokenExpireTime;
const deleteSession = async function(key){//returns token
   
    [error,success]= await to(RedisClient.del(key));
    if(success){
       return key;
      //console.log(success);
    }else{
      console.log(error);
    }
     
 }
 module.exports.deleteSession = deleteSession;
const createSessionForLogin = async function(userInfo){//returns token
   console.log(userInfo);
   var key = randomstring.generate(4);
   let org_id;
   if(userInfo.type=='employee'){
      org_id=userInfo.orginasation_id;
   }else{
      org_id=userInfo.id;
   }
   [error,success]= await to(RedisClient.hmset(key,"id",userInfo.id,"email",userInfo.email,"type",userInfo.type,"org_id",org_id));
   if(success){
    [error,success]= await to(RedisClient.lpush(userInfo.id,key));
      return key;
     //console.log(success);
   }else{
     console.log(error);
   }
    
}
module.exports.createSessionForLogin = createSessionForLogin;
const deletesessions = async function(id){//returns token
    let userSession = await RedisClient.lrange(id,0,-1);
    if(userSession){
       for(let i=0;i<userSession.length;i++){
           console.log(userSession[i]);
           RedisClient.del(userSession[i]);
       }
       RedisClient.del(id);
       return userSession;
    }else{
          return TE ("tocken expired or removed");
      //  console.log(error);
    }
}
module.exports.deletesessions=deletesessions;
const findSession = async function(key){//returns token
    [error,userSession]= await to(RedisClient.hgetall(key));
    if(userSession){
        return userSession;
    }else{
        return error;
    }
}
module.exports.findSession = findSession;
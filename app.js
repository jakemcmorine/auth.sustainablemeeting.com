require('./config/node_env');     //instantiate configuration variables
require('./global_functions');  //instantiate global functions
require('ejs');
//require('newrelic');




const express 		= require('express');
const logger 	    = require('morgan');
const bodyParser 	= require('body-parser');
const passport      = require('passport');
var cors = require('cors')
var path = require('path');
global.appRoot = path.resolve(__dirname);
const v1 = require('./routes/v1');

const app = express();
app.use(cors())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
const cookieParser = require('cookie-parser');
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

//Passport
//app.use(passport.initialize());


// parse application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//app.use(bodyParser.json({ type: 'application/*+json' }))

// parse some custom thing into a Buffer


//DATABASE
const models = require("./models");
const redisModels = require("./redis");

// CORS
// app.use(cors({
//   credentials: true,
//   origin: "http://localhost:3000"
// }));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    var allowedOrigins = [CONFIG.SUPERADMIN_URL,CONFIG.ADMIN_URL];
      var modifiedOrigin = allowedOrigins.map(url => url.substring(0, url.length - 1))
      console.log(modifiedOrigin)
      var origin = req.headers.origin;
      
      if(modifiedOrigin.indexOf(origin) > -1){
        res.setHeader('Access-Control-Allow-Origin', origin);
    } 
   // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3003')
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3003/inviteUsers')
   // res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, Content-Type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

//


app.use('/v1', v1);

app.use('/', function(req, res){
	res.statusCode = 200;//send the appropriate status code
	res.json({status:"success", message:"Parcel Pending API", data:{}})
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.send(err);
// });
//app.listen(3333);
module.exports = app;
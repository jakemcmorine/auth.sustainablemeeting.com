"use strict";
const {promisify} = require('util');
const  nodemailer  = require('nodemailer');
var ses = require('nodemailer-ses-transport');
const  ejs         = require('ejs');
const aws = require('aws-sdk');
const jwt           	= require('jsonwebtoken');

//var smtpTransport = nodemailer.createTransport();
class MailService {
	constructor(){
        this.smtpTransport = nodemailer.createTransport({
            port: 587,
            host: 'email-smtp.eu-west-1.amazonaws.com',
            secure: false,
            tls: {
              rejectUnauthorized: false
          },
           
            auth: {
              user: "AKIATXAUUPSJKASEALO5",
              pass: "BBF2pZL5cCkMX4CpByAW3O2RklKlMZT2ju+/mSnbg2zy",
            },
            debug: true
          });
		
        

    }
   async sendConfirmationEmail(user) {
   try{
            var sendMail = promisify(this.smtpTransport.sendMail).bind(this.smtpTransport);
            var ejsRender = promisify(ejs.renderFile).bind(ejs);
            var templatePath = appRoot+'/views/mails/registration.ejs';
            var sc = await ejsRender(templatePath, {username:user.email,password:user.original_password,resetLink:user.resetLink});
            if(sc){
                var mailOptions = {
                            from:"abin.joseph@emvigotech.com"  ,
                            to: user.email,
                            subject: "Set New Password",
                            html: sc
                        };
                return await sendMail(mailOptions);

            }

     }catch(error){
         console.log(error);
         throw error
     }
       
    }; 

    async sendEmailVarifyLink(key,user) {
        //     var token = user.passwordResetToken;
        console.log(user)
            try{
                var encryptKey= jwt.sign({user_id:key}, CONFIG.jwt_encryption,{expiresIn:30001})
                var sendMail = promisify(this.smtpTransport.sendMail).bind(this.smtpTransport);
                var ejsRender = promisify(ejs.renderFile).bind(ejs);
                var templatePath = appRoot+'/views/mails/emailVarify.ejs';
                let fname="";
                console.log(CONFIG.redirection)
                if(user.fname!=undefined){
                   fname=user.fname+",";
                }
                let sc=await ejsRender(templatePath, { link :CONFIG.redirection +'/email/varify/'+encryptKey,data:fname});
                if(sc){
                    var mailOptions = {
                                from:CONFIG.mail_from,
                                to: user.email,
                                subject: "Email varification",
                                html: sc
                            };
                    return await sendMail(mailOptions);
                }

            }catch(error){
                
                throw error;
            }
         };    
         async sendForgotPasswordLink(key,user) {
        //     var token = user.passwordResetToken;
        console.log(user)
            try{
                var encryptKey= jwt.sign({user_id:key}, CONFIG.jwt_encryption,{expiresIn:30001})
                var sendMail = promisify(this.smtpTransport.sendMail).bind(this.smtpTransport);
                var ejsRender = promisify(ejs.renderFile).bind(ejs);
                var templatePath = appRoot+'/views/mails/forgotPsd.ejs';
                let fname="";
                console.log(CONFIG.redirection)
                if(user.fname!=undefined){
                   fname=user.fname+",";
                }
                let sc=await ejsRender(templatePath, { link :CONFIG.redirection+'/change/psd/'+encryptKey,data:fname});
                if(sc){
                    var mailOptions = {
                                from:CONFIG.mail_from,
                                to: user.email,
                                subject: "Reset Password",
                                html: sc
                            };
                    return await sendMail(mailOptions);
                }

            }catch(error){
                
                throw error;
            }
         };
}
module.exports =  new MailService();